#include "polcario.h"

#include <QImage>
#include <QPainter>

QList<polcar::PolcarItem> polcar::PolcarIO::fromCsv(const QString & path)
{
    qDebug() << "[Polcar IO] Reading file" << path;

    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "[Polcar IO] File reading failed" << path;
        return QList <PolcarItem> ();
    }

    QList <PolcarItem> list;

    while (!file.atEnd()) {
        list << PolcarItem::fromCsv(file.readLine());
    }

    file.close();
    return list;
}

QList<polcar::PolcarItem> polcar::PolcarIO::fromUrls(const QString &path)
{
    qDebug() << "[Polcar IO] Reading file" << path;

    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "[Polcar IO] File reading failed" << path;
        return QList <PolcarItem> ();
    }

    QList <PolcarItem> list;

    while (!file.atEnd()) {
        PolcarItem item;
        const auto url = file.readLine().trimmed();

        const auto begin_pos = url.indexOf("#!") + 2;
        const auto end_pos = url.indexOf("/", begin_pos);
        item.categoryId = url.mid(begin_pos, end_pos - begin_pos);
        item.urlId = url.mid(url.lastIndexOf("/") + 1);
        item.url = url;

        list << item;
    }

    file.close();
    return list;
}

void polcar::PolcarIO::toCsv(polcar::PolcarItem &item, const QString & path)
{
    qDebug() << "[Polcar IO] Write cross csv data" << item.vendorCode;

    QByteArray crossDataCsv;
    for (auto crossIt = item.crossData.cbegin(); crossIt != item.crossData.cend(); ++crossIt) {
        const auto producer = crossIt.key();

        for (const auto & crossCode : crossIt.value()) {
            crossDataCsv.append("156;"); // id сайта по умолчанию
            crossDataCsv.append(item.vendorCode + ";");
            crossDataCsv.append(producer + ";");
            crossDataCsv.append(crossCode);
            crossDataCsv.append("\n");
        }
    }

    QFile file(path);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        file.write(crossDataCsv);
        file.close();
    }
}

void polcar::PolcarIO::log(polcar::PolcarItem &item, const QString &path)
{
    QFile file(path);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {

        QByteArray message;
        message.append(item.vendorCode + ";");
        message.append( (item.imageSmallPath.isEmpty()) ? "-" : item.imageSmallPath );
        message.append("\n");

        file.write(message);
        file.close();
    }
}

QString polcar::PolcarIO::validate_path(const QString &path)
{
    static const QRegExp regex("[" + QRegExp::escape( "\\/:*?\"<>|" ) + "]" );

    auto valid_path = path;
    valid_path = valid_path.replace(regex, "^");

    return valid_path;
}


polcar::PolcarItem polcar::PolcarItem::fromCsv(const QString &csv)
{
    PolcarItem item;

    const auto data = csv.split(";");
    item.site = data.at(0);
    item.vendorCode = data.at(1);

    return item;
}

void polcar::PolcarItem::buildImageFromImageParts()
{
    if (imageParts.isEmpty()) {
        return;
    }

    QList <QImage> images;

    for (auto & part : imageParts) {
        part = part.remove(0, part.indexOf(",") + 1);
        images << QImage::fromData(QByteArray::fromBase64(part.toLatin1()));
    }

    const auto widthRaw = images.first().width();
    const auto heightRaw = images.first().height();

    const auto width = widthRaw * 3;
    const auto height = heightRaw * 3;

    QImage image(QSize(width, height), QImage::Format::Format_RGB32);
    QPainter painter(&image);

    for (int i = 0; i < 9; i++) {
        const auto row = i / 3;
        const auto column = i % 3;

        const auto x = widthRaw * column;
        const auto y = heightRaw * row;

        painter.drawImage(x, y, images.at(i));
    }

    const auto path = QCoreApplication::applicationDirPath() + "/image/big/" +
                    PolcarIO::validate_path(producer);
    QDir().mkpath(path);

    const auto name = path + "/" + vendorCode + ".jpeg";
    image.save(name);
    imageBigPath = name;
}

QJsonObject polcar::PolcarItem::toJson() const
{
    QJsonObject json;

    // характеристики
    for (auto it = data.cbegin(); it != data.cend(); ++it) {
        json[it.key()] = it.value();
    }

    // картинки
    json["image_max"] = imageBigPath;
    json["image_min"] = imageSmallPath;

    json["category"] = categoryId;
    json["vendor"] = vendorCode;
    json["url"] = url;

    return json;
}

polcar::PolcarItem polcar::PolcarItem::fromHtml(const QString &html)
{
    PolcarItem item;

    item.imageUrl = PolcarFinder::findImageUrl(html);
    item.imageName = PolcarFinder::findImageName(item.imageUrl);
    item.partUrl = PolcarFinder::findPartUrl(html);

    auto [categoryId, vendorCodeRaw, urlId] = PolcarFinder::findPartUrlData(item.partUrl);
    item.categoryId = categoryId;
    item.vendorCodeFromSite = vendorCodeRaw;
    item.urlId = urlId;

            return item;
}

