#include <polcarwebclient.h>
#include <polcario.h>

#include <QCoreApplication>
#include <QtConcurrent>

using namespace polcar;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    const auto path = QCoreApplication::applicationDirPath() + "/polcar.txt";
    auto items = PolcarIO::fromUrls(path);

    QThreadPool::globalInstance()->setMaxThreadCount(50);

    PolcarWebClient polcar;
    polcar.updateCookie();

    QFile file(QCoreApplication::applicationDirPath() + "/result.csv");
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QtConcurrent::blockingMap(items, [&polcar] (auto & item) {

            polcar.productMoreInfo(item);

            if (!item.imageName.isEmpty()) {

                polcar.downloadImage(item);
                polcar.searchImageParts(item);

    //            if (!item.crossData.isEmpty()) {
    //                PolcarIO::toCsv(item, QCoreApplication::applicationDirPath() + "/cross.csv");
    //            }

            }

    //        PolcarIO::log(item, QCoreApplication::applicationDirPath() + "/log.csv");

        });

        for (const auto & item : items) {
            const auto line = item.imageBigPath + ";" +
                    QJsonDocument(item.toJson()).toJson(QJsonDocument::Compact) + "\n";
            file.write(line.toUtf8());
        }

        file.close();
    } else {
        qDebug() << "failed to open/create result file";
    }

    qDebug() << "parse finished";

    return a.exec();
}
