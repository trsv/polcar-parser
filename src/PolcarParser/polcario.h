#ifndef POLCARIO_H
#define POLCARIO_H

#include <polcarfinder.h>

#include <QtCore>

namespace polcar {

class PolcarItem {
public:
    QString vendorCode;
    QString site;

    QString vendorCodeFromSite;
    QString categoryId;
    QString urlId;

    QString producer;
    QString partUrl;

    QString imageUrl;
    QString imageName;
    QStringList imageParts;

    QString imageSmallPath;
    QString imageBigPath;

    QString url;

    QHash <QString, QStringList> crossData;
    QHash <QString, QString> data;

    static PolcarItem fromCsv(const QString & csv);
    static PolcarItem fromHtml(const QString & html);

    void buildImageFromImageParts();
    QJsonObject toJson() const;
};

class PolcarIO
{
public:
    static QList <PolcarItem> fromCsv(const QString &path);
    static QList <PolcarItem> fromUrls(const QString & path);
    static void toCsv(PolcarItem & item, const QString & path);
    static void log(PolcarItem & item, const QString & path);
    static QString validate_path(const QString & path);
};

}



#endif // POLCARIO_H
