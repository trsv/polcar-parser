#ifndef POLCARWEBCLIENT_H
#define POLCARWEBCLIENT_H

#include <polcario.h>
#include <polcarfinder.h>

#include <QtCore>
#include <QtNetwork>

namespace polcar {

class PolcarWebClient : public QObject
{
    Q_OBJECT

public:
    explicit PolcarWebClient(QObject * parent = nullptr);

public:
    bool updateCookie();
    void search(PolcarItem & item);
    void productMoreInfo(PolcarItem & item);
    void downloadImage(PolcarItem & item);
    void searchImageParts(PolcarItem & item);

    QList<QJsonObject> getUniversalModels(const QString &manufacter) const;
    QList<std::tuple<QString, QString>> getManufacturers() const;

private slots:
    std::tuple<bool, QByteArray> sendRequest(const QByteArray & method,  const QNetworkRequest & request,
                                const QByteArray & data = QByteArray()) const;

    QByteArray searchRequest(const QString &url, const QByteArray & data);

private:
    QStringList m_cookies_raw;
    mutable QHash <QString, QString> m_cookies_storage;

    QString m_csrfToken;
    mutable QString m_xsfrToken;

    mutable QMutex m_mutex;
};

}



#endif // POLCARWEBCLIENT_H
