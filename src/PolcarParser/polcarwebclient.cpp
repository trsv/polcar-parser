#include "polcarwebclient.h"

#include <QMutex>
#include <QMutexLocker>

polcar::PolcarWebClient::PolcarWebClient(QObject *parent) : QObject (parent)
{
}

bool polcar::PolcarWebClient::updateCookie()
{
    const auto [is_success, response] = sendRequest("GET", QNetworkRequest(QUrl("https://catalog.polcar.com/search")));

    if (!is_success) {
        qWarning() << "[Polcar Webclient] Update cookie failed";
        return false;
    }

    static const QRegularExpression regex("id=\"csrf-token\".*?content=\"(.*?)\"");

    const auto match = regex.match(response);
    if (match.hasMatch()) {
        m_csrfToken = match.captured(1);
    }

    return true;
}

void polcar::PolcarWebClient::search(PolcarItem & item)
{
    qDebug() << "[Polcar Webclient] Searching" << item.vendorCode;

    const QByteArray data("{\"query\":\"" + item.vendorCode.toUtf8() +"\"}");

    const auto searchResult = searchRequest("https://catalog.polcar.com/search", data);

    if (searchResult.isEmpty()) {
        return;
    }

    QJsonParseError error;
    const auto document = QJsonDocument::fromJson(searchResult, &error);

    if (error.error != QJsonParseError::NoError) {

        qWarning() << "[Polcar Webclient] Search response parse failed. Error:"
                   << error.errorString();
        return;
    }

    const auto response = document.object();
    const auto total = response["total"].toInt();

    if (total == 0) {
        return;
    }

    const auto html = response["html"].toString();

    for (const auto & itemResultRaw : PolcarFinder::findRawItems(html)) {

        auto itemResult = PolcarItem::fromHtml(itemResultRaw);

        if (itemResult.vendorCodeFromSite == item.vendorCode) {
            item = itemResult;
            item.vendorCode = item.vendorCodeFromSite;
            break;
        }
    }
}

void polcar::PolcarWebClient::productMoreInfo(polcar::PolcarItem &item)
{
    qDebug() << "[Polcar Webclient] Searching product more info" << item.vendorCode;

    QJsonObject json({
                         {"category", item.categoryId},
                         {"urlId", item.urlId},
                         {"isSearch", false},
                         {"isSelectable", true}
                     });

    const auto data = QJsonDocument(json).toJson(QJsonDocument::Compact);
    const auto searchResult = searchRequest("https://catalog.polcar.com/catalog/product", data);

    if (searchResult.isEmpty()) {
        qWarning() << "[Polcar Webclient] Get product info failed." << item.vendorCode;
        return;
    }

    QJsonParseError error;
    const auto document = QJsonDocument::fromJson(searchResult, &error);

    if (error.error != QJsonParseError::NoError) {

        qWarning() << "[Polcar Webclient] Search response parse failed. Error:"
                   << error.errorString();
        return;
    }

    const auto response = document.object();
    const auto html = response["content"].toString();

    item.producer = PolcarFinder::findProducer(html);
    item.data = PolcarFinder::findKeys(html);

    item.imageUrl = PolcarFinder::findImageUrl(html);
    item.imageName = PolcarFinder::findImageName(item.imageUrl);
    item.partUrl = PolcarFinder::findPartUrl(html);

    const auto title = response["title"].toString();
    item.vendorCode = title.mid(0, title.indexOf(" - "));
}

void polcar::PolcarWebClient::downloadImage(polcar::PolcarItem & item)
{
    qDebug() << "[Polcar Webclient] Download image" << item.vendorCode;

    QNetworkRequest request(QUrl("https://catalog.polcar.com/photos/" + item.imageName + "/normal"));
    request.setRawHeader("Referer", "https://catalog.polcar.com/search");
    request.setRawHeader("Accept", "image/webp,image/apng,image/*,*/*;q=0.8");

    QStringList cookies;
    for (auto it = m_cookies_storage.cbegin(); it != m_cookies_storage.end(); ++it) {
        cookies << it.key() + "=" + it.value();
    }

    request.setRawHeader("Cookie", cookies.join(";").toUtf8());

    const auto [is_success, response] = sendRequest("GET", request);

    if (is_success) {

        const auto path = QCoreApplication::applicationDirPath() + "/image/small/" +
                PolcarIO::validate_path(item.producer);

        QDir().mkpath(path);

        QFile file(path + "/" + item.vendorCode + ".jpeg");
        if (file.open(QIODevice::WriteOnly)) {
            file.write(response);
            file.close();

            item.imageSmallPath = file.fileName();
        }

    } else {
        qWarning() << "[Polcar Webclient] Download image failed" << item.vendorCode;
    }
}

void polcar::PolcarWebClient::searchImageParts(polcar::PolcarItem &item)
{
    static const auto pattern = QString("https://catalog.polcar.com/photos/%1/fragmented");

    QNetworkRequest request((QUrl(pattern.arg(item.imageName))));

    request.setRawHeader("Accept", "application/json, text/plain, */*");
    request.setRawHeader("X-XSRF-TOKEN", m_xsfrToken.toUtf8());
    request.setRawHeader("X-CSRF-TOKEN", m_csrfToken.toUtf8());
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("Referer", "https://catalog.polcar.com/search");

    QStringList cookies;
    for (auto it = m_cookies_storage.cbegin(); it != m_cookies_storage.end(); ++it) {
        cookies << it.key() + "=" + it.value();
    }

    request.setRawHeader("Cookie", cookies.join(";").toUtf8());

    const auto [is_success, response] = sendRequest("GET", request);

    if (is_success) {
        item.imageParts = polcar::PolcarFinder::findImageParts(response);
        item.buildImageFromImageParts();
    } else {
        qWarning() << "[Polcar Webclient] Getting image parts failed.";
    }
}

QList<QJsonObject> polcar::PolcarWebClient::getUniversalModels(const QString & manufacter) const
{
    static const auto pattern = "https://catalog.polcar.com/catalog/universal-models";

    QNetworkRequest request((QUrl(pattern)));

    request.setRawHeader("Accept", "application/json, text/plain, */*");
    request.setRawHeader("X-XSRF-TOKEN", m_xsfrToken.toUtf8());
    request.setRawHeader("X-CSRF-TOKEN", m_csrfToken.toUtf8());
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("Referer", "https://catalog.polcar.com/");
    request.setRawHeader("Content-Type", "application/json;charset=UTF-8");
    request.setRawHeader("Origin", "https://catalog.polcar.com");

    QStringList cookies;
    for (auto it = m_cookies_storage.cbegin(); it != m_cookies_storage.end(); ++it) {
        cookies << it.key() + "=" + it.value();
    }

    request.setRawHeader("Cookie", cookies.join(";").toUtf8());

    QJsonObject json;
    json["manufacturer_id"] = manufacter;

    const auto data = QJsonDocument(json).toJson(QJsonDocument::Compact);
    const auto [is_success, response] = sendRequest("POST", request, data);

    if (is_success) {
        const auto models = QJsonDocument::fromJson(response).array();

        QList <QJsonObject> list;
        for (const auto & modelRaw : models) {
            list << modelRaw.toObject();
        }
        return list;
    } else {
        qWarning() << "[Polcar Webclient] Getting models failed.";
        return QList <QJsonObject>();
    }
}

QList<std::tuple<QString, QString> > polcar::PolcarWebClient::getManufacturers() const
{
    const auto [is_success, response] = sendRequest("GET", QNetworkRequest(QUrl("https://catalog.polcar.com")));
    if (is_success) {
        const auto start_pos = response.indexOf("{\"initial");
        const auto finish_pos = response.indexOf(";", start_pos);
        const auto jsonRaw = response.mid(start_pos, finish_pos - start_pos);

        const auto json = QJsonDocument::fromJson(jsonRaw).object();
        const auto manufacturer = json["manufacturer"].toObject();
        const auto items = manufacturer["items"].toArray();

        QList<std::tuple<QString, QString>> list;
        for (const auto & itemRaw : items) {
            const auto item = itemRaw.toObject();
            list << std::make_tuple(item["id"].toString(), item["name"].toString());
        }
        return list;
    } else {
        qDebug() << "[Polcar Webclient] Getting manufacturer list failed";
        return QList<std::tuple<QString, QString>>();
    }
}

std::tuple<bool, QByteArray> polcar::PolcarWebClient::sendRequest(const QByteArray &method,
                                                    const QNetworkRequest &request,
                                                    const QByteArray &data) const
{
    QMutexLocker l(&m_mutex);

    QNetworkAccessManager network;
    auto reply = network.sendCustomRequest(request, method, data);

    QEventLoop loop;
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

    const auto new_cookies =
            qvariant_cast <QList <QNetworkCookie>> (reply->header(QNetworkRequest::SetCookieHeader));

    for (const auto & cookie : new_cookies) {

        if (cookie.name().toUpper() == "XSRF-TOKEN") {
            m_xsfrToken = cookie.value();
        }

        m_cookies_storage[cookie.name()] = cookie.value();
    }

    const auto is_success = reply->error() == QNetworkReply::NoError;
    const auto response = (reply->isReadable()) ? reply->readAll() : QByteArray();

    reply->deleteLater();
    return {is_success, response};
}

QByteArray polcar::PolcarWebClient::searchRequest(const QString & url, const QByteArray &data)
{
    QNetworkRequest request((QUrl(url)));
    request.setRawHeader("Origin", "https://catalog.polcar.com");
    request.setRawHeader("X-XSRF-TOKEN", m_xsfrToken.toUtf8());
    request.setRawHeader("X-CSRF-TOKEN", m_csrfToken.toUtf8());
    request.setRawHeader("Accept", "application/json, text/plain, */*");
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("Referer", "https://catalog.polcar.com/search");
    request.setRawHeader("Content-Type", "application/json;charset=UTF-8");

    QStringList cookies;
    for (auto it = m_cookies_storage.cbegin(); it != m_cookies_storage.end(); ++it) {
        cookies << it.key() + "=" + it.value();
    }

    request.setRawHeader("Cookie", cookies.join(";").toUtf8());

    const auto [is_success, response] = sendRequest("POST", request, data);

    if (!is_success) {
        qWarning() << "[Polcar Webclient] Search failed.";
    }

    return response;
}
