#ifndef POLCARFINDER_H
#define POLCARFINDER_H

#include <tuple>

#include <QtCore>

namespace polcar {

class PolcarFinder
{
public:
    static QStringList findRawItems(const QString & html);

    static QString findImageUrl(const QString & html);
    static QString findImageName(const QString & imageUrl);
    static QList<QString> findImageParts(const QByteArray & html);

    static QString findPartUrl(const QString & html);
    static std::tuple <QString, QString, QString> findPartUrlData(const QString & partUrl);

    static QString findProducer(const QString & html);
    static QHash<QString, QStringList> findCrossData(const QString & html);

    static QHash <QString, QString> findKeys(const QString & html);
};

}



#endif // POLCARFINDER_H
