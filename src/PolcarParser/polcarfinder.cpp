#include "polcarfinder.h"

QStringList polcar::PolcarFinder::findRawItems(const QString &html)
{
    auto match = QRegularExpression("<article").globalMatch(html);

    QStringList list;
    while (match.hasNext()) {
        const auto itemMatch = match.next();

        const auto lenght = html.indexOf("</article>", itemMatch.capturedEnd());
        list << html.mid(itemMatch.capturedStart(), lenght);
    }

    return list;
}

QString polcar::PolcarFinder::findImageUrl(const QString &html)
{
    return QRegularExpression("<img src=\"(.*?)\"").match(html).captured(1);
}

QString polcar::PolcarFinder::findImageName(const QString &imageUrl)
{
    const auto name = QRegularExpression("photos\\/(.*?)\\/thumbnail").match(imageUrl).captured(1);

    if (name.isEmpty()) {
        const auto end_pos = imageUrl.lastIndexOf("/");
        const auto begin_pos = imageUrl.lastIndexOf("/", end_pos - 1) + 1;
        return  imageUrl.mid(begin_pos, end_pos - begin_pos);
    } else {
        return name;
    }
}

QList<QString> polcar::PolcarFinder::findImageParts(const QByteArray &html)
{
    static const QRegularExpression regex("<td><img src=\"(.*?)\"><\\/td>");

    auto globalResultIterator = regex.globalMatch(html);

    QList<QString> list;
    while (globalResultIterator.hasNext()) {
        list << globalResultIterator.next().captured(1);
    }
    return list;
}

QString polcar::PolcarFinder::findPartUrl(const QString &html)
{
    return QRegularExpression("data-part-url=\"(.*?)\"").match(html).captured(1);
}

std::tuple<QString, QString, QString> polcar::PolcarFinder::findPartUrlData(const QString &partUrl)
{
    const auto match = QRegularExpression("#!(.*?)\\/.*-([^-]+)\\/(.*?)$").match(partUrl);
    return {match.captured(1), match.captured(2), match.captured(3)};
}

QString polcar::PolcarFinder::findProducer(const QString &html)
{
    auto indexStart = html.indexOf("Producer");

    if (indexStart == -1) {
        indexStart = html.indexOf("Manufacturer");
    }

    if (indexStart == -1) {
        indexStart = html.indexOf("Производитель");
    }

    if (indexStart == -1) {
        return "without-producer";
    }

    const auto indexKeyStart = html.indexOf("<td>", indexStart) + 4;
    const auto indexKeyEnd = html.indexOf("</td>", indexKeyStart);

    return html.mid(indexKeyStart, indexKeyEnd - indexKeyStart).trimmed();
}

QHash <QString, QStringList> polcar::PolcarFinder::findCrossData(const QString &html)
{
    const auto jsonRawData = QRegularExpression("cross-numbers-tabs :tree=\"(.*?)\"")
            .match(html).captured(1)
            .replace("&quot;", "\"")
            .replace("|separator|", ".");

    if (jsonRawData.isEmpty()) {
        return QHash <QString, QStringList>();
    }

    QJsonParseError error;
    const auto document = QJsonDocument::fromJson(jsonRawData.toUtf8(), &error);

    if (error.error != QJsonParseError::NoError) {
        return QHash <QString, QStringList>();
    }

    const auto crosses = document.object()["types"].toObject().toVariantHash();

    QHash <QString, QStringList> list;
    for (auto producerIterator = crosses.cbegin(); producerIterator != crosses.cend(); ++producerIterator) {
        const auto producer = producerIterator.key().trimmed();
        const auto vendorCodes = producerIterator.value()
                .toJsonObject()["numbers"]
                .toObject().toVariantHash();

        for (auto codeIterator = vendorCodes.cbegin(); codeIterator != vendorCodes.cend(); ++codeIterator) {
            list[producer] << codeIterator.key().trimmed();
        }

    }

    return list;
}

QHash<QString, QString> polcar::PolcarFinder::findKeys(const QString &html)
{
    static const QRegularExpression regex("<tr.*?> <td.*?>(.*?)<\\/td> <td.*?>(.*?)<\\/td> <\\/tr>");
    static const QRegExp cleaner("<[^>]*>");

    QHash<QString, QString> keys;
    auto globalResultIterator = regex.globalMatch(html.simplified());
    while (globalResultIterator.hasNext()) {

        const auto match = globalResultIterator.next();
        const auto key = match.captured(1).remove(cleaner).trimmed().toLower();
        const auto value = match.captured(2).remove(cleaner).trimmed();

        keys.insert(key, value);
    }

    return keys;
}

